from sqlalchemy.engine.reflection import Inspector

from datetime import datetime
import pytz
from tzlocal import get_localzone


local_tz = get_localzone()  # pytz.timezone('Europe/Moscow')


def now_tz(tz=local_tz):
    dt = datetime.now(pytz.utc)
    return dt.astimezone(tz)


def constraints(session, model):
    inspector = Inspector.from_engine(session.connection())
    return [cn for le in inspector.get_unique_constraints(model.__tablename__) for cn in le['column_names']]


def get_or_create(session, model, **kwargs):
    uc = constraints(session, model)
    if len(uc) > 0:
        df = {k: kwargs[k] for k in uc if k in kwargs}
    else:
        df = kwargs
    instance = session.query(model).filter_by(**df).first()
    if not instance:
        instance = model(**kwargs)
        session.add(instance)
        session.flush()
    return instance


def exec(session, name, *args, unnest=True):
    keys, pairs = list(), dict()
    for i, arg in enumerate(args):
        keys.append(':par' + str(i))
        if type(arg) in {set, tuple}:
            arg = list(arg)
        pairs['par' + str(i)] = arg
    rows = session.execute('SELECT * FROM ' + name + '(' + ','.join(keys) + ');', dict(pairs))
    if unnest and rows.rowcount == 1:
        for r in rows:
            if len(r) == 1:
                return list(r)[0]
            else:
                return dict(r)
    return [dict(r) for r in rows if r is not None]


