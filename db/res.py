res_data = {
    "site": [
        {
            "id": 1,
            "class_name": "FredPerry",
            "url": "https://www.fredperry.com/shop/",
            "title": "Fred Perry",
            "brand_id": 1,
            "country_id": 2
        },
        {
            "id": 2,
            "class_name": "NewBalance",
            "url": "http://www.newbalance.com/",
            "title": "New Balance",
            "brand_id": 2,
            "country_id": 3
        }
    ],
    "country": [
        {
            "id": 1,
            "title_en": "Russia",
            "title_ru": "Россия"
        },
        {
            "id": 2,
            "title_en": "United Kingdom",
            "title_ru": "Великобритания"
        },
        {
            "id": 3,
            "title_en": "United States",
            "title_ru": "США"
        }
    ],
    "currency": [
        {
            "id": 1,
            "code": "RUR",
            "title_en": "Rouble",
            "title_ru": "Рубль",
            "country_id": 1,
            "buy": 1,
            "sell": 1
        },
        {
            "id": 2,
            "code": "GBP",
            "title_en": "Pound sterling",
            "title_ru": "Фунт стерлингов",
            "country_id": 2
        },
        {
            "id": 3,
            "code": "USD",
            "title_en": "United States dollar",
            "title_ru": "Доллар США",
            "country_id": 3
        }
    ],
    "brand": [
        {
            "id": 1,
            "title_en": "Fred Perry",
            "country_id": 2
        },
        {
            "id": 2,
            "title_en": "New Balance",
            "country_id": 3
        }
    ],
    "category": [
        {
            "id": 1,
            "super_id": 0,
            "title_ru": "Одежда",
            "title_en": "Clothing",

            "attributes": 0
        },
        {
            "id": 2,
            "super_id": 0,
            "title_ru": "Обувь",
            "title_en": "Shoe",
            "title_plr_ru": "Обувь",
            "title_plr_en": "Shoes",
            "attributes": 0
        },
        {
            "id": 3,
            "super_id": 0,
            "title_ru": "Аксессуар",
            "title_en": "Accessory",
            "title_plr_ru": "Аксессуары",
            "title_plr_en": "Accessories",
            "attributes": 0
        },
        {
            "id": 4,
            "super_id": 0,
            "title_ru": "Сумка",
            "title_en": "Bag",
            "title_plr_ru": "Сумки",
            "title_plr_en": "Bags",
            "attributes": 0
        },
        {
            "id": 5,
            "super_id": 1,
            "title_ru": "Плечевая",
            "title_en": "Upper body",
            "attributes": 1
        },
        {
            "id": 6,
            "super_id": 1,
            "title_ru": "Поясная",
            "title_en": "Waist",
            "attributes": 1
        },
        {
            "id": 7,
            "super_id": 5,
            "title_ru": "Рубашка",
            "title_en": "Shirt",
            "title_plr_ru": "Рубашки",
            "title_plr_en": "Shirts",
            "attributes": 0
        },
        {
            "id": 8,
            "super_id": 5,
            "title_ru": "Рубашка с коротким рукавом",
            "title_en": "Short sleeve shirt",
            "title_plr_ru": "Рубашки с коротким рукавом",
            "title_plr_en": "Short sleeve shirts",
            "attributes": 0
        },
        {
            "id": 9,
            "super_id": 5,
            "title_ru": "Блузка",
            "title_en": "Blouse",
            "title_plr_ru": "Блузки",
            "title_plr_en": "Blouses",
            "attributes": 0
        },
        {
            "id": 10,
            "super_id": 5,
            "title_ru": "Футболка",
            "title_en": "T-shirt",
            "title_plr_ru": "Футболки",
            "title_plr_en": "T-shirts",
            "attributes": 0
        },
        {
            "id": 11,
            "super_id": 5,
            "title_ru": "Майка",
            "title_en": "Sleeveless shirt",
            "title_plr_ru": "Майки",
            "title_plr_en": "Sleeveless shirts",
            "attributes": 0
        },
        {
            "id": 12,
            "super_id": 5,
            "title_ru": "Топ",
            "title_en": "Top",
            "title_plr_ru": "Топы",
            "title_plr_en": "Tops",
            "attributes": 0
        },
        {
            "id": 13,
            "super_id": 5,
            "title_ru": "Поло",
            "title_en": "Polo",
            "title_plr_en": "Polos",
            "attributes": 0
        },
        {
            "id": 14,
            "super_id": 5,
            "title_ru": "Трикотаж",
            "title_en": "Knitwear",
            "attributes": 0
        },
        {
            "id": 15,
            "super_id": 14,
            "title_ru": "Свитер",
            "title_en": "Sweater",
            "title_plr_ru": "Свитеры",
            "title_plr_en": "Sweaters",
            "attributes": 0
        },
        {
            "id": 16,
            "super_id": 14,
            "title_ru": "Кофта",
            "title_en": "Cardigan",
            "title_plr_ru": "Кофты",
            "title_plr_en": "Cardigans",
            "attributes": 0
        },
        {
            "id": 17,
            "super_id": 5,
            "title_ru": "Толстовка",
            "title_en": "Zip hoodie",
            "title_plr_ru": "Толстовки",
            "title_plr_en": "Zip hoodies",
            "attributes": 0
        },
        {
            "id": 18,
            "super_id": 5,
            "title_ru": "Худи",
            "title_en": "Hoodie",
            "title_plr_en": "Hoodies",
            "attributes": 0
        },
        {
            "id": 19,
            "super_id": 5,
            "title_ru": "Верхняя одежда",
            "title_en": "Outwear",
            "attributes": 0
        },
        {
            "id": 20,
            "super_id": 19,
            "title_ru": "Куртка",
            "title_en": "Jacket",
            "title_plr_ru": "Куртки",
            "title_plr_en": "Jackets",
            "attributes": 0
        },
        {
            "id": 21,
            "super_id": 19,
            "title_ru": "Спортивная куртка",
            "title_en": "Track jacket",
            "title_plr_ru": "Спортивные куртки",
            "title_plr_en": "Track jackets",
            "attributes": 0
        },
        {
            "id": 22,
            "super_id": 19,
            "title_ru": "Пальто",
            "title_en": "Coat",
            "title_plr_en": "Coats",
            "attributes": 0
        },
        {
            "id": 23,
            "super_id": 5,
            "title_ru": "Пиджак",
            "title_en": "Jacket",
            "title_plr_ru": "Пиджаки",
            "title_plr_en": "Jackets",
            "attributes": 0
        },
        {
            "id": 24,
            "super_id": 5,
            "title_ru": "Блейзер",
            "title_en": "Blazer",
            "title_plr_ru": "Блейзеры",
            "title_plr_en": "Blazers",
            "attributes": 0
        },
        {
            "id": 25,
            "super_id": 5,
            "title_ru": "Платье",
            "title_en": "Dress",
            "title_plr_ru": "Платья",
            "title_plr_en": "Dresses",
            "attributes": 0
        },
        {
            "id": 26,
            "super_id": 5,
            "title_ru": "Комбинезон",
            "title_en": "Jumpsuit",
            "title_plr_ru": "Комбинезоны",
            "title_plr_en": "Jumpsuits",
            "attributes": 0
        },
        {
            "id": 27,
            "super_id": 6,
            "title_ru": "Брюки",
            "title_en": "Trousers",
            "attributes": 0
        },
        {
            "id": 28,
            "super_id": 25,
            "title_ru": "Джинсы",
            "title_en": "Jeans",
            "attributes": 0
        },
        {
            "id": 29,
            "super_id": 6,
            "title_ru": "Шорты",
            "title_en": "Shorts",
            "title_plr_ru": "Шорты",
            "title_plr_en": "Shorts",
            "attributes": 0
        },
        {
            "id": 30,
            "super_id": 6,
            "title_ru": "Юбка",
            "title_en": "Skirt",
            "title_plr_ru": "Юбки",
            "title_plr_en": "Skirts",
            "attributes": 0
        },
        {
            "id": 31,
            "super_id": 0,
            "title_ru": "Пляжная",
            "title_en": "Swimwear",
            "attributes": 0
        },
        {
            "id": 32,
            "super_id": 2,
            "title_ru": "Сапоги",
            "title_en": "Boot",
            "title_plr_en": "Boots",
            "attributes": 0
        },
        {
            "id": 33,
            "super_id": 2,
            "title_ru": "Ботинки",
            "title_en": "Ankle boot",
            "title_plr_en": "Ankle boots",
            "attributes": 0
        },
        {
            "id": 34,
            "super_id": 2,
            "title_ru": "Туфли",
            "title_en": "Dress shoe",
            "title_plr_en": "Dress shoes",
            "attributes": 0
        },
        {
            "id": 35,
            "super_id": 2,
            "title_ru": "Кроссовки",
            "title_en": "Sneaker",
            "title_plr_en": "Sneakers",
            "attributes": 0
        },
        {
            "id": 36,
            "super_id": 2,
            "title_ru": "Спортивные кроссовки",
            "title_en": "Running",
            "attributes": 0
        },
        {
            "id": 37,
            "super_id": 2,
            "title_ru": "Кеды",
            "title_en": "Tennis shoe",
            "title_plr_en": "Tennis shoes",
            "attributes": 0
        },
        {
            "id": 38,
            "super_id": 2,
            "title_ru": "Мокасины",
            "title_en": "Boat shoe",
            "title_plr_en": "Boat shoes",
            "attributes": 0
        },
        {
            "id": 39,
            "super_id": 2,
            "title_ru": "Лоферы",
            "title_en": "Loafer",
            "title_plr_en": "Loafers",
            "attributes": 0
        },
        {
            "id": 40,
            "super_id": 2,
            "title_ru": "Шлепанцы",
            "title_en": "Flip-flop",
            "title_plr_en": "Flip-flops",
            "attributes": 0
        },
        {
            "id": 41,
            "super_id": 2,
            "title_ru": "Сандалии",
            "title_en": "Sandal",
            "title_plr_en": "Sandals",
            "attributes": 0
        },
        {
            "id": 42,
            "super_id": 2,
            "title_ru": "Туфли",
            "title_en": "Pump",
            "title_plr_en": "Pumps",
            "attributes": 0
        },
        {
            "id": 43,
            "super_id": 2,
            "title_ru": "Балетки",
            "title_en": "Ballet shoe",
            "title_plr_en": "Ballet shoes",
            "attributes": 0
        },
        {
            "id": 44,
            "super_id": 3,
            "title_ru": "Ремень",
            "title_en": "Belt",
            "title_plr_ru": "Ремни",
            "title_plr_en": "Belts",
            "attributes": 0
        },
        {
            "id": 45,
            "super_id": 3,
            "title_ru": "Шапка",
            "title_en": "Hat",
            "title_plr_ru": "Шапки",
            "title_plr_en": "Hats",
            "attributes": 0
        },
        {
            "id": 46,
            "super_id": 3,
            "title_ru": "Кепка",
            "title_en": "Cap",
            "title_plr_ru": "Кепки",
            "title_plr_en": "Caps",
            "attributes": 0
        },
        {
            "id": 47,
            "super_id": 3,
            "title_ru": "Панама",
            "title_en": "Bucket hat",
            "title_plr_ru": "Панамы",
            "title_plr_en": "Bucket hats",
            "attributes": 0
        },
        {
            "id": 48,
            "super_id": 3,
            "title_ru": "Шарф",
            "title_en": "Scarf",
            "title_plr_ru": "Шарфы",
            "title_plr_en": "Scarves",
            "attributes": 0
        },
        {
            "id": 49,
            "super_id": 3,
            "title_ru": "Перчатки",
            "title_en": "Glove",
            "title_plr_en": "Gloves",
            "attributes": 0
        },
        {
            "id": 50,
            "super_id": 3,
            "title_ru": "Кошелёк",
            "title_en": "Wallet",
            "title_plr_ru": "Кошельки",
            "title_plr_en": "Wallets",
            "attributes": 0
        },
        {
            "id": 51,
            "super_id": 3,
            "title_ru": "Носки",
            "title_en": "Socks",
            "attributes": 0
        }
    ]
}
