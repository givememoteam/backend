from sqlalchemy import create_engine, Column, Integer, Text, Sequence, ForeignKey, text as dbtext, event
from sqlalchemy.dialects.postgresql import ARRAY, VARCHAR, NUMERIC, BIGINT, TIMESTAMP, INTERVAL
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship, scoped_session
from .res import res_data

engine = create_engine('postgresql://postgres:8765309241@localhost:5432/gmm_staging_2', echo=False)
Session = scoped_session(sessionmaker())
Session.configure(bind=engine)
Base = declarative_base()


def fill_data(data, model):
    session = Session()
    data_list = data[model.__tablename__]
    session.add_all([model(**d) for d in data_list])
    session.commit()
    session.close()


country_id_seq = Sequence('country_id_seq', start=1)
currency_id_seq = Sequence('currency_id_seq', start=1)
brand_id_seq = Sequence('brand_id_seq', start=1)
care_id_seq = Sequence('care_id_seq', start=1)
tag_id_seq = Sequence('tag_id_seq', start=1)
site_id_seq = Sequence('site_id_seq', start=1)
site_page_id_seq = Sequence('site_page_id_seq', start=1)
site_item_id_seq = Sequence('site_item_id_seq', start=1)
product_id_seq = Sequence('product_id_seq', start=1)
collection_id_seq = Sequence('collection_id_seq', start=1)
color_id_seq = Sequence('color_id_seq', start=1)
material_id_seq = Sequence('material_id_seq', start=1)
image_id_seq = Sequence('image_id_seq', start=1)


class Country(Base):
    __tablename__ = 'country'
    id = Column(Integer, country_id_seq, primary_key=True, server_default=country_id_seq.next_value())
    super_id = Column(Integer)
    title_en = Column(Text)
    title_ru = Column(Text)


@event.listens_for(Country.__table__, 'after_create')
def fill_country_table(target, connection, **kw):
    fill_data(res_data, Country)
    
    
class Currency(Base):
    __tablename__ = 'currency'
    id = Column(Integer, currency_id_seq, primary_key=True, server_default=currency_id_seq.next_value())
    code = Column(VARCHAR(length=5), unique=True, nullable=False)
    title_en = Column(Text)
    title_ru = Column(Text)
    country_id = Column(Integer, ForeignKey('country.id'))
    country = relationship('Country', foreign_keys=[country_id])
    buy = Column(NUMERIC)
    sell = Column(NUMERIC)
    updated = Column(TIMESTAMP(timezone=True))


@event.listens_for(Currency.__table__, 'after_create')
def fill_country_table(target, connection, **kw):
    fill_data(res_data, Currency)
    engine.execute("""
    CREATE OR REPLACE FUNCTION public.before_currency_update()
      RETURNS trigger
    AS
    $$
    BEGIN
      NEW.updated := CURRENT_TIMESTAMP;
      RETURN NEW;
    END
    $$
    LANGUAGE plpgsql;

    CREATE TRIGGER t_before_currency_update
    BEFORE UPDATE ON currency
    FOR EACH ROW
    WHEN (OLD.buy IS DISTINCT FROM NEW.buy OR OLD.sell IS DISTINCT FROM NEW.sell)
    EXECUTE PROCEDURE before_currency_update();""")


class Brand(Base):
    __tablename__ = 'brand'
    id = Column(Integer, brand_id_seq, primary_key=True, server_default=brand_id_seq.next_value())
    super_id = Column(Integer)
    title_en = Column(Text)
    title_ru = Column(Text)
    country_id = Column(Integer, ForeignKey('country.id'))
    country = relationship('Country', foreign_keys=[country_id])


@event.listens_for(Brand.__table__, 'after_create')
def fill_brand_table(target, connection, **kw):
    fill_data(res_data, Brand)


class Tag(Base):
    __tablename__ = 'tag'
    id = Column(Integer, tag_id_seq, primary_key=True, server_default=tag_id_seq.next_value())
    super_id = Column(Integer)
    redirect_id = Column(Integer)
    title_ru = Column(Text)
    title_en = Column(Text, nullable=False, unique=True)
    plural_ru = Column(Text)
    plural_en = Column(Text)
    attributes = Column(Integer, nullable=False, server_default=dbtext('0'))


@event.listens_for(Tag.__table__, 'after_create')
def create_tag_ids_getter(target, connection, **kw):
    engine.execute("""
    CREATE OR REPLACE FUNCTION public.get_tag_ids(_tag_vls text[][])
      RETURNS integer[]
    AS
    $$
      DECLARE Rec RECORD;
      DECLARE _tag_ids INT[][];
      DECLARE _tag_id INT;
      DECLARE _title_en text;
      DECLARE _plural_en text;
    BEGIN
      FOR Rec IN
        SELECT
          unnest(_tag_vls[1:10][1:1]) sng,
          unnest(_tag_vls[1:10][2:2]) plr
      LOOP
        _title_en := Rec.sng;
        _plural_en := Rec.plr;
        SELECT id INTO _tag_id FROM tag WHERE title_en = _title_en;
        if _tag_id IS NULL THEN
          INSERT INTO tag
            (title_en, plural_en)
          VALUES
            (_title_en, _plural_en)
          RETURNING
            id INTO _tag_id;
        END IF;
        _tag_ids := array_append(_tag_ids, _tag_id);
      END LOOP;
      RETURN _tag_ids;
    END
    $$
    LANGUAGE plpgsql;""")


class Site(Base):
    __tablename__ = 'site'
    id = Column(Integer, site_id_seq, primary_key=True, server_default=site_id_seq.next_value())
    class_name = Column(Text, nullable=False, unique=True)
    url = Column(Text)
    title = Column(Text)
    brand_id = Column(Integer, ForeignKey('brand.id'))
    brand = relationship('Brand', foreign_keys=[brand_id])
    country_id = Column(Integer, ForeignKey('country.id'))
    country = relationship('Country', foreign_keys=[country_id])
    page_lifetime = Column(INTERVAL, server_default=dbtext("interval '1 day'"))     # '00:00:30'
    item_lifetime = Column(INTERVAL, server_default=dbtext("interval '02:00:00'"))  # '00:00:30'


@event.listens_for(Site.__table__, 'after_create')
def fill_site_table(target, connection, **kw):
    fill_data(res_data, Site)


class SitePage(Base):
    __tablename__ = 'site_page'
    id = Column(Integer, site_page_id_seq, primary_key=True, server_default=site_page_id_seq.next_value())
    site_id = Column(Integer, ForeignKey('site.id'), nullable=False)
    site = relationship('Site', foreign_keys=[site_id])
    url = Column(Text, nullable=False, unique=True)
    gender_id = Column(Integer)
    tag_ids = Column(ARRAY(Integer))
    collection_id = Column(Integer, ForeignKey('collection.id'), nullable=True)
    collection = relationship('Collection', foreign_keys=[collection_id])
    title_en = Column(Text)
    attributes = Column(Integer, nullable=False, server_default=dbtext('0'))
    added = Column(TIMESTAMP(timezone=True), server_default=dbtext('now()'))
    checked = Column(TIMESTAMP(timezone=True))


class SiteItem(Base):
    __tablename__ = 'site_item'
    id = Column(Integer, site_item_id_seq, primary_key=True, server_default=site_item_id_seq.next_value())
    site_page_id = Column(Integer, ForeignKey('site_page.id'), nullable=False)
    site_page = relationship('SitePage', foreign_keys=[site_page_id])
    code = Column(VARCHAR(length=50), unique=True, nullable=False)
    group_code = Column(VARCHAR(length=50))
    url = Column(Text, nullable=False)
    product_id = Column(Integer, ForeignKey('product.id'))
    product = relationship('Product', foreign_keys=[product_id])
    title_en = Column(Text)
    size_vls = Column(ARRAY(VARCHAR(length=10), dimensions=2))
    attributes = Column(Integer, nullable=False, server_default=dbtext('0'))
    price = Column(NUMERIC)
    price_std = Column(NUMERIC)
    added = Column(TIMESTAMP(timezone=True), server_default=dbtext('now()'))
    checked = Column(TIMESTAMP(timezone=True))


class Product(Base):
    __tablename__ = 'product'
    id = Column(Integer, product_id_seq, primary_key=True, server_default=product_id_seq.next_value())
    brand_id = Column(Integer, ForeignKey('brand.id'))
    brand = relationship('Brand', foreign_keys=[brand_id])
    gender_id = Column(Integer)
    tag_ids = Column(ARRAY(Integer))
    collection_id = Column(Integer, ForeignKey('collection.id'), nullable=True)
    collection = relationship('Collection', foreign_keys=[collection_id])
    title_en = Column(Text)
    title_ru = Column(Text)
    care_id = Column(Integer, ForeignKey('care.id'))
    care = relationship('Care', foreign_keys=[care_id])
    barcode = Column(BIGINT)
    weight = Column(NUMERIC)
    attributes = Column(Integer, nullable=False, server_default=dbtext('0'))
    color_ids = Column(ARRAY(Integer))
    material_ids = Column(ARRAY(Integer, dimensions=2))


class Collection(Base):
    __tablename__ = 'collection'
    id = Column(Integer, collection_id_seq, primary_key=True, server_default=collection_id_seq.next_value())
    brand_id = Column(Integer, ForeignKey('brand.id'))
    brand = relationship('Brand', foreign_keys=[brand_id])
    title_en = Column(Text, nullable=False)
    title_ru = Column(Text)
    added = Column(TIMESTAMP(timezone=True), server_default=dbtext('now()'))


class Color(Base):
    __tablename__ = 'color'
    id = Column(Integer, color_id_seq, primary_key=True, server_default=color_id_seq.next_value())
    super_id = Column(Integer)
    title_en = Column(Text, nullable=False, unique=True)
    title_ru = Column(Text)
    hex = Column(Integer)


@event.listens_for(Color.__table__, 'after_create')
def create_color_ids_getter(target, connection, **kw):
    engine.execute("""
    CREATE OR REPLACE FUNCTION public.get_color_ids(_color_vls text[])
      RETURNS int[]
    AS
    $$
      DECLARE Rec RECORD;
      DECLARE _color_ids INT[];
      DECLARE _color_id INT;
      DECLARE _title_en TEXT;
    BEGIN
      FOR Rec IN
        SELECT cv FROM unnest(_color_vls) cv
      LOOP
        _title_en := trim(Rec.cv);
        SELECT id INTO _color_id FROM color WHERE title_en = _title_en;
        IF _color_id IS NULL THEN
          INSERT INTO color (title_en) VALUES (_title_en) RETURNING id INTO _color_id;
        END IF;
        _color_ids := array_append(_color_ids, _color_id);
      END LOOP;
      RETURN _color_ids;
    END
    $$
    LANGUAGE plpgsql;
    """)


class Material(Base):
    __tablename__ = 'material'
    id = Column(Integer, material_id_seq, primary_key=True, server_default=material_id_seq.next_value())
    super_id = Column(Integer)
    title_en = Column(Text, nullable=False, unique=True)
    title_ru = Column(Text)


@event.listens_for(Material.__table__, 'after_create')
def create_material_ids_getter(target, connection, **kw):
    engine.execute("""
    CREATE OR REPLACE FUNCTION public.get_material_ids(_material_vls text[][])
      RETURNS integer[][]
    AS
    $$
      DECLARE Rec RECORD;
      DECLARE _material_ids INT[][];
      DECLARE _material_id INT;
      DECLARE _title_en text;
    BEGIN
      FOR Rec IN
        SELECT
          unnest(_material_vls[1:10][1:1]) a,
          unnest(_material_vls[1:10][2:2]) b
      LOOP
        _title_en := trim(Rec.b);
        SELECT id INTO _material_id FROM material WHERE title_en = _title_en;
        if _material_id IS NULL THEN
          INSERT INTO material
            (title_en)
          VALUES
            (_title_en)
          RETURNING
            id INTO _material_id;
        END IF;
        _material_ids := _material_ids || ARRAY[[CAST(Rec.a as int), _material_id]];
      END LOOP;
      RETURN _material_ids;
    END
    $$
    LANGUAGE plpgsql;""")


class Care(Base):
    __tablename__ = 'care'
    id = Column(Integer, care_id_seq, primary_key=True, server_default=care_id_seq.next_value())
    super_id = Column(Integer)
    title_en = Column(Text, nullable=False, unique=True)
    title_ru = Column(Text)


class Image(Base):
    __tablename__ = 'image'
    id = Column(Integer, image_id_seq, primary_key=True, server_default=image_id_seq.next_value())
    product_id = Column(Integer, ForeignKey('product.id'))
    product = relationship('Product', foreign_keys=[product_id])
    source_url = Column(Text, nullable=False, unique=True)


Base.metadata.create_all(engine)
