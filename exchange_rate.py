from db import Session, Currency
import requests
import json


def update():
    session = Session()
    js = json.loads(requests.get('https://www.tinkoff.ru/api/v1/currency_rates/', 'html5lib').text)
    for rate in [r for r in js['payload']['rates']
                 if r['category'] == 'DebitCardsOperations' and r['toCurrency']['name'] == 'RUB']:
        session.query(Currency).filter(Currency.code == rate['fromCurrency']['name']).\
            update({'buy': rate['buy'], 'sell': rate['sell']})
    session.commit()
    session.close()
    return

