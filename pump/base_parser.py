from requests import get as requests_get
from swiss import *
from bs4 import BeautifulSoup
from db import SitePage, SiteItem, Tag, Collection, Care, Image, exec, get_or_create
import re
import json


class BaseParser:

    def __init__(self):
        self.__cache__ = dict()

    def get(self, url):
        if url not in self.__cache__:
            response = requests_get(url, 'html5lib')
            if not response.ok:
                self.__cache__[url] = None
            else:
                self.__cache__[url] = response.text
        return self.__cache__[url]

    def get_page_list(self, session, site):
        return

    def get_item_list(self, session, site_page):
        return False

    def get_item_info(self, session, site_item):
        return False

    def get_product_info(self, session, site_item):
        return


def to_tags(tag_list):
    tl = list()
    for t in tag_list:
        t = titleize(t)
        tl.append([singularize(t), pluralize(t)])
    return tl


def to_colors(s, sep='/'):
    return uniqulize([titleize(cs) for cs in s.split(sep)])


def to_materials(material_list):
    if len(material_list) == 0:
        return None
    ml = list()
    for ms in [sub.split(':')[-1] for sub in material_list]:
        if ms.find('%') in range(0, len(ms) - 2):
            sl = ms.split('%')
            percent = sl[0].strip()
            material = titleize(sl[1])
        else:
            percent = '0'
            material = titleize(ms)
        ml.append([percent, material])
    return ml
