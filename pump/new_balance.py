from .base_parser import *

api_url_mask = '{}on/demandware.store/Sites-newbalance_us2-Site/en_US/Product-GetVariants?pid={}'
sale_url_mask = '{}recently-reduced/{}-recently-reduced-shoes/{}/'
sale_gender_titles = ['mens', 'womens']

nb_materials = ['Foam', 'EVA', 'Rubber', 'Syntetic', 'PU', 'Lycra', 'Ortholite', 'Suede', 'Textile', 'TPU',
                'Microfiber', 'Nylon', 'Nubuck', 'Metal', 'Vibram', 'Polyurethane', 'Leather']
nb_mat_alter = {'imeva': 'Foam', 'acteva': 'Foam', 'c-cap': 'EVA', 'revlite': 'EVA', 'ndurance': 'Carbon Rubber',
                'abzorb': 'Synthetic Rubber', 'encap': 'EVA', 'n-ergy': 'Thermoplastic Gel', 'slim-lok': 'Nylon',
                'polyurethane': 'PU', 'rollbar': 'TPU'}


def get_api_url(site_item):
    return api_url_mask.format(site_item.site_page.site.url, site_item.group_code)


class NewBalance(BaseParser):

    def get_page_list(self, session, site):
        for gender_id in {1, 2}:
            for nav in BeautifulSoup(self.get(site.url), 'html5lib').\
                    find_all('li', class_='category-nav category-' + str(gender_id*1000)):
                for a in nav.find('div', class_='subcategory ').find_all('a', class_='subCat'):
                    title_en = titleize(a.text)
                    if title_en != 'Recently Reduced Shoes':
                        tag_vls = list()
                        for t in [ts for ts in title_en.split('&') if ts != '']:
                            if t.find('Shoes') < 0 and t != 'Sandals':
                                ts = singularize(t) + ' Shoes'
                            else:
                                ts = t
                            tag_vls.append(ts)
                        tag_ids = exec(session, 'get_tag_ids', to_tags(tag_vls))
                        tu = parameterize(title_en)
                        for url in [a['href'], sale_url_mask.format(site.url, sale_gender_titles[gender_id-1], tu)]:
                            get_or_create(session, SitePage, url=url, tag_ids=tag_ids, gender_id=gender_id,
                                          collection_id=None, title_en=title_en, site_id=site.id)
        return

    def get_item_list(self, session, site_page):
        prod_tile = BeautifulSoup(self.get(site_page.url), 'html5lib').find_all('div', class_='product product-tile ')
        for div in prod_tile:
            title_en = titleize(div.find('p', class_='product-name').text)
            for clr in div.find('div', class_='swatches').find_all('a', class_='color'):
                get_or_create(session, SiteItem, code=clr['title'], url=clr['href'], title_en=title_en,
                              group_code=div['data-master-id'], site_page_id=site_page.id)
        return len(prod_tile) > 0

    def get_item_info(self, session, site_item):
        js = json.loads(self.get(get_api_url(site_item)))
        vr = [v for v in js['variants'] if v['styleNo'] == site_item.code and v['availability']['inStock']]
        if len(vr) > 0:
            site_item.price = numerize(vr[0]['pricemodel']['pricing']['salesPriceFormatted'])
            if 'standardPriceFormatted' in vr[0]['pricemodel']['pricing']:
                site_item.price_std = numerize(vr[0]['pricemodel']['pricing']['standardPriceFormatted'])
            else:
                site_item.price_std = site_item.price
            site_item.size_vls = [[v['attributes']['size']['value'], v['attributes']['width']['value']] for v in vr]
        return len(vr) > 0

    def get_product_info(self, session, site_item):
        js = json.loads(self.get(get_api_url(site_item)))['styles'][site_item.code]
        color_vls = uniqulize([titleize(js['primaryGenericColor']), js['secondaryGenericColor']])
        site_item.product.color_ids = exec(session, 'get_color_ids', color_vls)
        for img in js['zoomimages']:
            get_or_create(session, Image, source_url=img['URL'], product_id=site_item.product.id)
        js = json.loads(re.findall('\<script type="application\/ld\+json"\>([\s\S]*?)\<\/script\>',
                                   self.get(site_item.url))[0], strict=False)
        if 'weight' in js:
            site_item.product.weight = numerize(js['weight'].split(' ')[0]) / 1000
        bs_text = BeautifulSoup(self.get(site_item.url), 'html5lib').text
        ml = [m for m in nb_materials if contain_word(m, bs_text)] + \
             [nb_mat_alter[m] for m in nb_mat_alter if contain_word(m, bs_text)]
        if len(ml) > 0:
            site_item.product.material_ids = exec(session, 'get_material_ids', to_materials(ml))
        return
