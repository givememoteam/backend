from .base_parser import *


class FredPerry(BaseParser):

    def get_page_list(self, session, site):
        for l1 in BeautifulSoup(self.get(site.url), 'html5lib').\
                find('div', class_='panel left-panel').find_all('li', class_='level1'):
            for idx in [i for i, c in enumerate(['Men', 'Women', 'Collections', 'Sale']) if c == l1.find('a').text]:
                if idx < 3:
                    if idx == 2:
                        aa = [a for a in l1.find_all('a', class_='level3') if a.text.find('All') < 0]
                    else:
                        aa = [a for a in l1.find_all('a', class_='level2') if a.text.find('All') < 0]
                    for a in aa:
                        title_en = titleize(a.text)
                        if idx != 2:
                            tag_ids = exec(session, 'get_tag_ids', to_tags(title_en.split('&')))
                            collection_id = None
                        else:
                            tag_ids = None
                            collection_id = get_or_create(session, Collection, title_en=titleize(a.text),
                                                          brand_id=site.brand_id).id
                        gender_id = int(a['href'].find('women') >= 0) + 1
                        get_or_create(session, SitePage, url=a['href'].strip(), tag_ids=tag_ids, gender_id=gender_id,
                                      collection_id=collection_id, title_en=title_en, site_id=site.id)
                else:
                    pass
        return

    def get_item_list(self, session, site_page):
        info = [l for l in BeautifulSoup(self.get(site_page.url), 'html5lib').find_all('div', class_='product-info')
                if 'double' not in l['class']]
        for li in info:
            url = li.find('a')['href']
            js = json.loads(li.find('div', class_='defacto-gtm-item-impression')['data-impression'], strict=False)
            code = js['id'].strip()
            title_en = titleize(js['name'])
            get_or_create(session, SiteItem, code=code, url=url, title_en=title_en, site_page_id=site_page.id)
        return len(info) > 0

    def get_item_info(self, session, site_item):
        conf = re.findall('Product.Config\((.*)\);', self.get(site_item.url))
        if len(conf) > 0:
            js = json.loads(conf[0], strict=False)
            site_item.price = js['basePrice']
            site_item.price_std = js['oldPrice']
            for k in js['attributes'].keys():
                site_item.size_vls = [[l['label'].split('(')[0].strip(), ''] for l in js['attributes'][k]['options']]
        return len(conf) > 0

    def get_product_info(self, session, site_item):
        js = json.loads(re.findall('\<script type="application\/ld\+json"\>([\s\S]*?)\<\/script\>',
                                   self.get(site_item.url))[0], strict=False)
        site_item.product.color_ids = exec(session, 'get_color_ids', to_colors(js['color']))
        soup = BeautifulSoup(self.get(site_item.url), 'html5lib')
        for idx, li in enumerate(reversed(soup.find('div', class_='product-shop').find_all('li'))):
            if idx == 1:
                site_item.product.care_id = get_or_create(session, Care, title_en=titleize(li.text)).id
            elif idx == 2:
                site_item.product.material_ids = exec(session, 'get_material_ids', to_materials(li.text.split('/')))
        for img in soup.find('div', class_='slides').find_all('img'):
            get_or_create(session, Image, source_url=img['src'], product_id=site_item.product.id)
        return


'''sale_cat = {3: 1256, 4: 1261, 19: 1254, 23: 1263, 2: 1252, 14: 1255, 13: 2112, 30: 1269, 17: 1268, 10: 1258,
            21: 1270, 27: 1319, 7: 1257}    # Извлекать теги распродажи автоматически?
sale_gen = {1: 1225, 2: 1222}

a = l1.find('a')
for gender_id in self.sale_gen:
    for category_id in self.sale_cat:
        url = a['href'] + '?fp_category={}&fp_gender={}'.format(
            self.sale_cat[category_id], self.sale_gen[gender_id])
        upsert(session, SitePage, url=url, title=a.text, gender_id=gender_id,
               category_id=category_id, site_id=site.id)'''
