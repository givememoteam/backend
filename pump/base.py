import sys
from db import Session, Site, SitePage, SiteItem, Product, now_tz
from sqlalchemy import or_
from .fred_perry import *
from .new_balance import *
from sentry import sentry_client


def start(site_id=None):
    session = Session()
    try:
        if not site_id:
            q = session.query(Site)
            print('Начато скачивание данных со всех сайтов')
        else:
            q = session.query(Site).filter(Site.id == site_id)
            print('Начато скачивание данных с сайта №{}'.format(site_id))
        for site in q:
            try:
                parser = globals()[site.class_name]()
                if parser.get(site.url):
                    parser.get_page_list(session, site)
                    session.commit()
                    page_update_deadline = now_tz() - site.page_lifetime
                    item_update_deadline = now_tz() - site.item_lifetime
                    for site_page in session.query(SitePage). \
                            filter(SitePage.site_id == site.id). \
                            filter(SitePage.attributes.op('&')(1) != 1). \
                            order_by(SitePage.checked):
                        try:
                            if (site_page.checked is None) or (site_page.checked < page_update_deadline):
                                if (not parser.get(site_page.url)) or (not parser.get_item_list(session, site_page)):
                                    site_page.attributes |= 1
                                else:
                                    site_page.checked = now_tz()
                                session.commit()
                            for site_item in session.query(SiteItem). \
                                    filter(SiteItem.site_page_id == site_page.id). \
                                    filter(SiteItem.attributes.op('&')(1) != 1). \
                                    filter(or_(SiteItem.checked == None, SiteItem.checked < item_update_deadline)). \
                                    order_by(SiteItem.checked):
                                try:
                                    if (not parser.get(site_item.url)) or (
                                    not parser.get_item_info(session, site_item)):
                                        site_item.attributes |= 1
                                    else:
                                        if (site.brand_id is not None) and (site.brand_id > 0) and \
                                                (site_item.product_id is None):
                                            product = Product(title_en=site_item.title_en,
                                                              brand_id=site.brand_id,
                                                              tag_ids=site_item.site_page.tag_ids,
                                                              collection_id=site_item.site_page.collection_id,
                                                              gender_id=site_item.site_page.gender_id)
                                            session.add(product)
                                            session.flush()
                                            site_item.product_id = product.id
                                            parser.get_product_info(session, site_item)
                                            print('Добавлен новый товар - {}'.format(product.title_en))
                                        site_item.checked = now_tz()
                                    session.commit()
                                except:
                                    sentry_client.captureException()
                        except:
                            sentry_client.captureException()
            except:
                sentry_client.captureException()
    finally:
        session.commit()
        session.close()
        print('Скачивание данных завершено')
    return
