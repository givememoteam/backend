import re


def uniqulize(l):
    return [item for idx, item in enumerate(l) if item not in l[:idx] and item is not None]


def numerize(s):
    sf = re.sub('[^0-9\.]', '', s)
    if sf != '':
        return float(sf)
    else:
        return float(0)


def contain_word(word, text):
    return re.search(r'\b(?i){}\b'.format(word), text) is not None
